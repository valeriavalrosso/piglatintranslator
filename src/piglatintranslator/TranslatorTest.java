package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

//il traduttore prende una frase come input -> il client la ottiene dal traduttore
public class TranslatorTest {

	@Test
	public void testInputPhrase() {		//1
		String inputPhrase = "hello world";	//frase 
		
		Translator translator = new Translator(inputPhrase);	//creo classe Translator (in src) e passo la frase in input
		
		assertEquals("hello world", translator.getPhrase());		//per ottenere l'output ottenuto metodo translator. getphrase per ottenere la frase
	}
	
	@Test
	public void testTranslationEmptyPhrase() {	//2
		String inputPhrase = "";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL, translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartinWithVowelEndingWithY() {	//3
		String inputPhrase = "any";		//termina con y -> nay alla fine della parola
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("anynay", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartinWithUEndingWithY() {
		String inputPhrase = "utility";		
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("utilitynay", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";		//termina con una vowel -> yay alla fine della parola
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("appleyay", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";		//termina con una consonante -> ay alla fine della parola
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("askay", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartingWithASingleConsonant() {	//4
		String inputPhrase = "hello";		
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay", translator.translate());		
	}
		
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {		//5	
		String inputPhrase = "known";		
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay", translator.translate());		
	}
	

	@Test
	public void testTranslationPhraseMoreWordsWithSpaces() {	//6		
		String inputPhrase = "hello world";	
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", translator.translate());		
	}

	
	@Test
	public void testTranslationPhraseMoreWordsWithHyphen() {		
		String inputPhrase = "well-being";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay", translator.translate());		
	}
	
	
	@Test
	public void testTranslationPhraseWithPunctuations() {	//7
		String inputPhrase = "hello world!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!", translator.translate());		
	}

/*
	@Test
	public void testTranslationPhraseWithUpperWords() {		//8 
		String inputPhrase = "APPLE";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("APPLEAY", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseWithTitleCaseWords() {		
		String inputPhrase = "Hello";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("Ellohay", translator.translate());		
	}

*/	

}
