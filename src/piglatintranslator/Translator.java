package piglatintranslator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Translator {
	 
	public final static String NIL = "nil";		//2 traduzione dell'empty phrase � NIL
	
	private List <String> phrase = new ArrayList<String>();
	
	public Translator(String inputPhrase)
	{
	
				String str[] = inputPhrase.split(" ");
				phrase = Arrays.asList(str);
			
	}
	
	public String getPhrase()
	{
		
		String frase = "";
				
		for (String singleWord : phrase) {
			frase+=singleWord+" ";
		}
		
		return frase.substring(0, frase.length() - 1);
				
		
	}

	public String translate() {
				
		String result=this.NIL;
		
		if(!this.getPhrase().equals("")) {
		
		result="";
			
		for (String string : phrase) {
			
			
			if(string.contains("-")) {		//6.2
			List <String> temp = new ArrayList<String>();
			String str[] = string.split("-");
			temp = Arrays.asList(str);
			
			for (String tempWord : temp) {
				result+=prepareWords(tempWord);
				if(!temp.get(temp.size()-1).equals(tempWord)) {
					result+="-";
				}
				
				
			} 
			
			break;
		}
			
			//7 aggiungo punto, virgola, punto e virgola, due punti, domanda, punto esclamativo, apostrofo e parentesi tonda alla fine
			
			if(string.endsWith("!")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + "!";								
				continue;				
			}
			if(string.endsWith(".")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + ".";
				continue;
			}
			if(string.endsWith(",")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + ",";
				continue;
			}
			if(string.endsWith(";")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + ";";
				continue;
			}
			if(string.endsWith(":")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + ":";
				continue;
			}
			if(string.endsWith("?")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + "?";
				continue;
			}
			if(string.endsWith("'")) {
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + "'";
				continue;
			}						
			if(string.endsWith(")")) {	
				String newString = string.substring(0, string.length()-1);
				result += prepareWords(newString) + ")";
				continue;
			}			

			
			result+=prepareWords(string);
			if(!phrase.get(phrase.size()-1).equals(string)) {
				result+=" ";
			}
			
					
		}
				
		
		}
				

		return result;
	}
	
	public String prepareWords(String word) {		
		
		 if(startWithVowel(word)){		//3
			if(word.endsWith("y")) {
				return word + "nay";
			}else if(endWithVowel(word)) {
				return word + "yay";
			}else if(!endWithVowel(word)) {
				return word + "ay";
			}	
		}
		

			for(int i=0; i< word.length(); i++) {		//4 e 5
				if(checkVowel(word.substring(i,i+1))==true) {
							return word.substring(i) + word.substring(0,i) + "ay";
					}					
				}
			
		
										
		return word;
		
	}
	
	private boolean startWithVowel(String word) {	//metodo per verificare se inizia con una vocale, restituisce un boolean
		return(word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u"));
	}
	
	private boolean endWithVowel(String word) {
		return(word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u"));
	}
	
	private boolean checkVowel(String letter) {
		return(letter.endsWith("a") || letter.startsWith("e") || letter.endsWith("i") || letter.endsWith("o") || letter.endsWith("u"));
	}
	
}
